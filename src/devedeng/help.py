#!/usr/bin/env python3

# Copyright 2014 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of DeVeDe-NG
#
# DeVeDe-NG is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License.
#
# DeVeDe-NG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


from gi.repository import Gtk, Gdk
import devedeng.configuration_data
import os


class help:

    def __init__(self, help_page):

        self.config = devedeng.configuration_data.configuration.get_config()

        file = "file://" + \
            os.path.join(self.config.help_path, "html", help_page)

        retval = Gtk.show_uri(None, file, Gdk.CURRENT_TIME)
        if retval == False:
            msg = devede_dialogs.show_error(
                gladefile, _("Can't open the help files."))
